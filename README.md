# Item Price Watcher

## How to run:
1. Using Docker, run the container `selenium/standalone-firefox` bound to port 4444
    ``` bash
    docker run -d -p 4444:4444 selenium/standalone-firefox
    ```
2. Navigate to the folder ItemPriceWatcher and execute `dotnet run`